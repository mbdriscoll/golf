"""
Question 1: Palidrome detection.

This function should return true if `word' is a palindrome,
and false otherwise.
"""
def is_palindrome(w):
  return False


"""
Question 2: Vowel Counting.

This function should return the number of vowels in `word'. For our purposes,
'y' is never considered a vowel.
"""
def count_vowels(w):
  return 0


"""
Question 3: Parity

This function should return true if and only if there are an odd
number of 1's in the list.

Hint: you may want to type() your input
"""
def has_odd_parity(binary_list):
    return False


"""
Question 4: Alternating capitalizer.

This function should return the given word with every other letter
capitalized, starting with the first.
"""
def alternating_capitalize(word):
    return word

"""
Question 5: Is Triangle

Return true if all three edges in the input list could make
a valid triangle

@param edge_list : list of three edges
Hint: This is not Pythagoreus's domain
"""
def is_triangle(edge_list):
    return False


"""
Question 6: Traffic lights.

This function should return the color that follows the given color.
Assume a GREEN -> YELLOW -> RED -> green sequence.
"""
def next_color(color):
    return 'GREEN'


"""
Question 7: Unique Lists

This function should return True if the given list has unique
elements (e.g. no repeats), and false otherwise.
"""
def has_unique_elements(l):
    return False


"""
Question 8: Fibonacci numbers.

This function should return the n'th Fibonacci number.
fib(0) -> 0
fib(1) -> 1
fib(2) -> 1
fib(3) -> 2
fib(4) -> 3
fib(5) -> 5
"""
def fib(n):
    return 0

"""
Question 9: Quote Generator.

Write a function that returns the following phrase.
"""
def sing():
    return """Let it go, let it go. I am one with the wind and sky. Let it go, let it go. You'll never see me cry."""




"""
Challenge Question A:

Write a function that takes in an integer and returns a four-tuple
of values between 0 and 255 for the first three, and values between 0 and 1
for the last entry

e.g. an acceptible output is (0, 255, 174, 0.5)

These correspond to red, green, blue and alpha values for an image.
"""
def rgba(i):
    r = i*2 % 255
    g = 255
    b = 178
    a = 1
    return (r, g, b, a)

#----------------------------------------------------------------------------
# Don't worry about the stuff down here /jedi-hand-wave

import unittest
from inspect import getsource, getdoc
from textwrap import dedent

class TestGolf(unittest.TestCase):
    def test_q1(self):
        for word, answer in [
            ('hello',       False), ('racecar',     True),
            ('panama',      False), ('costarica',   False),
        ]:
            self.assertEqual( is_palindrome(word), answer,
                "Expected is_palindrome('%s') to return %s not %s" % (word, answer, is_palindrome(word)))

    def test_q2(self):
        for word, answer in [
            ('hello',   2), ('racecar', 3),
            ('rhythm',  0), ('yellow',  2),
            ('Orange',  3),
        ]:
            self.assertEqual( count_vowels(word), answer,
                "Expected count_vowels('%s') to return %d not %s" % (word, answer, count_vowels(word)))

    def test_q3(self):
        for word, answer in [
            ('0',       False), ('1',       True),
            ('01',      True),  ('10',      True),
            ('00',      False), ('11',      False),
            ('1010010', True),
        ]:
            lst = [int(c) for c in word]
            self.assertEqual( has_odd_parity(lst), answer,
                "Expected has_odd_parity('%s') to return %s not %s" % (word, answer, has_odd_parity(lst)))

    def test_q4(self):
        for word, answer in [
            ('hello', 'HeLlO'),
            ('pixel', 'PiXeL'),
            ('banana', 'BaNaNa'),
            ('A', 'A'),
            ('a', 'A'),
            ('hi', 'Hi'),
            ('hI', 'Hi'),
        ]:
            self.assertEqual( alternating_capitalize(word), answer ,
            "Expected alternating_capitalize(%s), to return %s not %s" % (word, answer, alternating_capitalize(word)))

    def test_q5(self):
        pass
        for edge_list, answer in [
            ([10, 7, 12], True), ([3,4,5], True),
            ([1,1,1], True),     ([0,4,5], False),
            ([6, 8, 20], False), ([9.0, 14.2, 13.4], True)
        ]:
            self.assertEqual(is_triangle(edge_list), answer,
                 "Expected is_triangle(%s) to return %s not %s" % (edge_list, answer, is_triangle(edge_list)))

    def test_q6(self):
        for first, second in [
            ('RED', 'GREEN'),
            ('GREEN', 'YELLOW'),
            ('YELLOW', 'RED'),
        ]:
            self.assertEqual( next_color(first), second,
                 "Expected next_color(%s) to return %s not %s" % (first, second, next_color(first)))

    def test_q7(self):
        for lst, answer in [
            ([1, 2, 3], True),
            ([1, 2, 1], False),
            ([0, 0, 0], False),
            ([0],       True),
            ([1],       True),
            ([1, 4, 5], True),
        ]:
          self.assertEqual( has_unique_elements(lst), answer,
                 "Expected has_unique_elements(%s) to return %s not %s" % (lst, answer, has_unique_elements(lst)))

    def test_q8(self):
        for n, fibn in [
            (0, 0),    (4, 3),
            (1, 1),    (5, 5),
            (2, 1),    (6, 8),
            (3, 2),    (7, 13),
        ]:
          self.assertEqual( fib(n), fibn,
                 "Expected fib(%s) to return %s not %s" % (n, fibn, fib(n)))


    def test_q9(self):
        expected = """Let it go, let it go. I am one with the wind and sky. Let it go, let it go. You'll never see me cry."""
        actual = sing()
        self.assertEqual(actual, expected,
             "Expected sing() to return %s not %s" % (expected, actual))


def main():
    totalChars = 0
    print '='*10 + " Scoring " + '='*10
    for i, q in enumerate([
            is_palindrome,
            count_vowels,
            has_odd_parity,
            alternating_capitalize,
            is_triangle,
            next_color,
            has_unique_elements,
            fib,
            sing,
    ]):
        text = "".join(dedent(getsource(q)).split())
        totalChars += len(text)
        print "Question %1d: %4d characters" % (i+1, len(text))
    print     "Total:      %4d characters" % totalChars
    print '='*29

    unittest.main()

def challenge():
    from PIL import Image
    im = Image.new('RGB', (1024, 1024))
    im.putdata([rgba(i) for i, item in enumerate(im.getdata())])
    im.show()

if __name__ == '__main__':
    main()

    # To run the challenge, uncomment the code below
    # challenge()

